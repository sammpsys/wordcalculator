# wordcalculator

A program that prompts the user for a mathematical sentence where numbers are in the format number operation number and returns the result of said operation. 

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Developers](#developers)

## Requirements

Requires `gcc`.

## Usage

Compile wordcalc.cpp and run. Program will prompt user for input and user must supply three words in the following order: number operation number. Any deviation from the specified format will result in program displaying an error message and reprompting user. Numbers must be in word form (i.e. one, two, three etc.) and the valid range is zero to twenty. Operations are addition, subtraction, multiplication and division. Format for operations is plus, minus, times and over. Examples for each are provided. 

## Addition

![](.github/images/plus.jpg)

Subtraction

![](.github/imagesminus.jpg)

Multiplication

![](.github/imagestimes.jpg)

Division
![](.github/imagesover.jpg)


## Author

Sam Nassiri

